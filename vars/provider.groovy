#!/usr/bin/env groovy

import com.provider.Util

def call(body) {
    def myHump = false
    node {
        config = [:]
        body.resolveStrategy = Closure.DELEGATE_FIRST
        body.delegate = config
        body()

        

        stage ("MyHump") {
            echo "myHump is $myHump"
            myHump = true
            echo "myHump is $myHump"
        }

        stage ("MySecondHump") {
            echo "myHump is $myHump"
            myHump = !myHump
            echo "myHump is $myHump"
        }

        Util util = new Util();
        
        echo "$config"

        echo "Hello from lib"

        echo util.sayHello("Vlad")

        echo "Hi"
    }
}